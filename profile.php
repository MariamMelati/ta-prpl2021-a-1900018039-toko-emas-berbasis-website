<?php 

    // Mmemanggil connect database
    include '../connect.php';

    session_start();

    if (empty($_SESSION['id_admin'])) {
        header('location:logout.php');
    }

    $queryProfile    = "SELECT * FROM profil WHERE id_profil='1'";
    $rowProfile      = mysqli_query($koneksinya, $queryProfile);
    $resultProfile   = mysqli_fetch_assoc($rowProfile);

    if (isset($_POST['selesai'])) {
        $deskripsi  = $_POST['deskripsi'];

        $queryEdit  = mysqli_query($koneksinya, "UPDATE profil SET deskripsi='$deskripsi' WHERE id_profil='1'");

        if (!empty($queryEdit)) {
            header('location:profile.php?page=profile');
        }
    }

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= $_SESSION['nama']; ?> - Edit Profile</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <?php require 'sidebar.php'; ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php require 'top-bar.php'; ?>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Edit Profile</h1>
                    </div>

                    <!-- Content Row -->
                    <div class="row">

                        <!-- Earnings (Monthly) Card Example -->
                        <div class="col-md-12">
                            <div class="card border-left-primary shadow h-100 py-2">
                                <form action="" method="POST" class="col-12" enctype="multipart/form-data">
                                    <div class="p-5">
                                        <div class="text-center">
                                            <h1 class="h4 text-primary mb-4">Masukkan data di bawah ini dengan lengkap & benar!</h1>
                                        </div>
                                        <div class="form-group">
                                            <label for="deskripsi">Deskripsi Jasa</label>
                                            <textarea id="deskripsi" class="ckeditor" name="deskripsi" required><?= $resultProfile['deskripsi']; ?></textarea>
                                        </div>
                                        <button type="submit" name="selesai" class="btn btn-primary btn-user btn-block">SELESAI <i class="fa fa-check"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Your Website 2021</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

    <script src="ckeditor/ckeditor.js"></script>

</body>

</html>