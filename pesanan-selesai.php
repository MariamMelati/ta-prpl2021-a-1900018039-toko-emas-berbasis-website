<?php 

    // Mmemanggil connect database
    include '../connect.php';
    include '../rp.php';

    session_start();

    if (empty($_SESSION['id_admin'])) {
        header('location:logout.php');
    }

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= $_SESSION['nama']; ?> - Pesanan Selesai</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
    <!-- Custom styles for this page -->
    <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <?php require 'sidebar.php'; ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php require 'top-bar.php'; ?>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Pesanan Selesai</h1>
                    </div>

                    <!-- Content Row -->
                    <div class="row">
                        <!-- DataTales Example -->
                        <div class="col-12 card shadow mb-4">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>ID Transaksi</th>
                                                <th>Nama Produk</th>
                                                <th>Jumlah</th>
                                                <th>Total Bayar</th>
                                                <th>Waktu Pesan</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php

                                                $nomor = 1;
                                                $queryTransaksi    = "SELECT transaksi.id_transaksi, transaksi.id_pembeli, transaksi.jumlah, transaksi.total_bayar, transaksi.status, transaksi.waktu, produk.nama_produk FROM transaksi INNER JOIN produk ON transaksi.id_produk = produk.id_produk WHERE status='Selesai' ORDER BY transaksi.waktu DESC";
                                                $rowTransaksi      = mysqli_query($koneksinya, $queryTransaksi);
                                                while ($resultTransaksi   = mysqli_fetch_assoc($rowTransaksi)) {

                                            ?>

                                            <tr>
                                                <td><?= $nomor++; ?></td>
                                                <td><?= $resultTransaksi['id_transaksi']; ?></th>
                                                <td><?= $resultTransaksi['nama_produk']; ?></td>
                                                <td><?= $resultTransaksi['jumlah']; ?></td>
                                                <td><strong class="text-success">Rp<?= rp($resultTransaksi['total_bayar']); ?></strong></td>
                                                <td><?= $resultTransaksi['waktu']; ?></td>
                                                <td class="text-center">
                                                    <a href="#" class="btn btn-block btn-success">
                                                        <i class="fas fa-clipboard-check"></i> Selesai
                                                    </a>
                                                </td>
                                            </tr>

                                            <?php } ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Your Website 2021</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="js/demo/datatables-demo.js"></script>

</body>

</html>