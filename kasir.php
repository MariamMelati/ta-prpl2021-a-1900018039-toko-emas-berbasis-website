<?php 

    // Mmemanggil koneksi databas 
    include '../../ini-koneksi-database.php';

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>MELATI</title>
    
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
   
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
   
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
   
    <link rel="stylesheet" href="../dist/css/skins/skin-blue.min.css">

   
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  
    <header class="main-header">

       
        <a href="index.php" class="logo">
            
            <span class="logo-mini"><b>ADM</b></span>
    
            <span class="logo-lg"><b>Admin</b></span>
        </a>

        <nav class="navbar navbar-static-top" role="navigation">
           
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
        </nav>
    </header>


    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header">Menu Utama</li>
                <!-- Optionally, you can add icons to the links -->
                <li class="active"><a href="index.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
                <li><a href="kasir.php"><i class="fa fa-calculator"></i> <span>Kasir</span></a></li>
                <li><a href="barang.php"><i class="fa fa-list"></i> <span>Barang</span></a></li>
                <li><a href="pegawai.php"><i class="fa fa-list"></i> <span>Pegawai</span></a></li>
                <li><a href="pembeli.php"><i class="fa fa-list"></i> <span>Pembeli</span></a></li>
            </ul>
          <!-- /.sidebar-menu -->
        </section>
    <!-- /.sidebar -->
    </aside>

    
    <div class="content-wrapper">

       
        <section class="content">

            <div class="box box-primary">

                <div class="box-body">

                    <div class="alert alert-info alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-info"></i> SISTEM KASIR!</h4>
                        Mohon di isi dengan lengkap.
                    </div>

                    <!-- enctype="multipart/form-data" adalah sebuah function untuk upload gambar -->
                    <form action="proses_kasir.php" method="POST" role="form">

                        <div class="form-group">
                            <label for="id_transaksi">id transaksi</label>
                            <input type="number" class="form-control" id="id_transaksi" name="id_transaksi" placeholder="Cth: 100222" required>
                        </div>

                        <div class="form-group">
                            <label for="id_barang">Nama Barang</label>
                            <select class="form-control" name="id_barang" id="id_barang">
                                <?php 

                                    $querybarang    = "SELECT * FROM barang ORDER BY id_barang DESC";
                                    $rowsbarang     = mysqli_query($koneksi, $querybarang);
                                    while ($resultbarang   = mysqli_fetch_assoc($rowsbarang)) {

                                ?>
                                <option value="<?php echo $resultbarang['id_barang']; ?>"><?php echo $resultbarang['nama_barang']; ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="id_pembeli">Nama Pembeli</label>
                            <select class="form-control" name="id_pembeli" id="id_pembeli">
                                <?php 

                                    $querypembeli    = "SELECT * FROM pembeli ORDER BY id_pembeli DESC";
                                    $rowspembeli     = mysqli_query($koneksi, $querypembeli);
                                    while ($resultpembeli   = mysqli_fetch_assoc($rowspembeli)) {

                                ?>
                                <option value="<?php echo $resultpembeli['id_pembeli']; ?>"><?php echo $resultpembeli['nama_pembeli']; ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="tgl_transaksi">tgl transaksi</label>
                            <input type="date" class="form-control" id="tgl_transaksi" name="tgl_transaksi" required>
                        </div>

                        <div class="form-group">
                            <label for="totalItem">Total</label>
                            <input type="number" class="form-control" id="totalItem" name="totalItem" placeholder="Cth: 5" required>
                        </div>

                        <button type="submit" name="tambah" class="btn btn-success">Selesai</button>
                    </form>
                </div>
            </div>

        </section>
        
    </div>
    
    <footer class="main-footer">
        
        <div class="pull-right hidden-xs">
            Anything you want
        </div>
       
        <strong>Copyright &copy; 2016 <a href="#">Company</a>.</strong> All rights reserved.
    </footer>

</div>

<script src="../plugins/jQuery/jquery-2.2.3.min.js"></script>

<script src="../bootstrap/js/bootstrap.min.js"></script>

<script src="../dist/js/app.min.js"></script>


</body>
</html>
