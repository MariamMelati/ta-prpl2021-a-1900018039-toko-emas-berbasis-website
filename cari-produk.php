<?php 

    // Mmemanggil connect database
    include 'connect.php';
    include 'rp.php';

    session_start();

    if (empty($_SESSION['nama'])) {
        $namaNya    = "Pembeli";
    }else{
        $namaNya    = $_SESSION['nama'];
    }

    if (!isset($_POST['cariproduk'])) {
        header('location:index.php');
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>PRODUK TOKO EMAS - <?= $namaNya; ?></title>
    <link rel="stylesheet" href="plugins/css/bootstrap.css">
    <link rel="stylesheet" href="plugins/css/heroic-features.css">
    <link rel="stylesheet" href="plugins/css/font-awesome.min.css">
</head>

<body class="bg-light">

    <!-- Navigation -->
    <?php require 'navigation.php'; ?>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Features -->
        <div class="row text-center my-4">

            <div class="col-12 my-4">
                <h3>Produk yang anda cari: <u class="text-primary"><?= $_POST['input_cari']; ?></u></h3>
            </div>

            <?php

                if (isset($_POST['cariproduk'])) {

                    $nama_produk    = $_POST['input_cari'];

                    $queryProduk    = "SELECT * FROM produk WHERE nama_produk LIKE '%$nama_produk%' ORDER BY id_produk DESC";
                    $rowProduk      = mysqli_query($koneksinya, $queryProduk);
                    while ($resultProduk   = mysqli_fetch_assoc($rowProduk)) {

            ?>

            <div class="col-lg-3 col-md-6 mb-4">
                <div class="card h-100">
                    <img class="card-img-top" src="gambar-produk/<?= $resultProduk['gambar']; ?>" alt="<?= $resultProduk['nama_produk']; ?>">
                    <div class="card-body">
                        <h4 class="card-title"><?= $resultProduk['nama_produk']; ?></h4>
                        <p class="card-text">Stok: <?= $resultProduk['stock']; ?></p>
                        <h5 class="card-title text-primary">Rp<?= rp($resultProduk['harga']); ?></h5>
                    </div>
                    <div class="card-footer">
                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#beliProduk<?= $resultProduk['id_produk']; ?>">Beli Sekarang</a>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="beliProduk<?= $resultProduk['id_produk']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <form class="modal-content" action="pesanan-saya.php" method="POST">
                        <div class="modal-header">
                            <h5 class="modal-title text-success" id="exampleModalLabel">Beli produk</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body text-left">
                            <h5 class="text-primary">Apakah anda yakin ingin membeli produk <br /> <u><?= $resultProduk['nama_produk']; ?></u>?</h5>
                            <div class="form-group mt-4">
                                <label for="jumlah" class="text-success font-weight-bold">Masukkan jumlah beli</label>
                                <input type="number" class="form-control" id="jumlah" name="jumlah" min="1" max="<?= $resultProduk['stock']; ?>" value="1">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal <i class="fa fa-close"></i></button>
                            
                            <input type="hidden" name="id_produk" value="<?= $resultProduk['id_produk']; ?>">
                            <input type="hidden" name="harga" value="<?= $resultProduk['harga']; ?>">
                            <button type="submit" name="pesan_produk" class="btn btn-success">Ya, Saya Beli <i class="fa fa-cart-plus"></i></button>

                        </div>
                    </form>
                </div>
            </div>

            <?php
                    }
                }
            ?>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <?php require 'footer.php'; ?>

    <script src="plugins/js/jquery.min.js"></script>
    <script src="plugins/js/popper.js"></script> 
    <script src="plugins/js/bootstrap.min.js"></script>

    <script type="text/javascript">

        // Popover
        $(document).ready(function(){
            $('[data-toggle="popover"]').popover();   
        });
        // Popover

    </script>

</body>
</html>