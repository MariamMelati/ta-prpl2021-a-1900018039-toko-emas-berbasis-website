<?php 

    // Mmemanggil connect database
    include 'connect.php';
    include 'rp.php';

    date_default_timezone_set('Asia/Jakarta');

    session_start();

    if (empty($_SESSION['nama'])) {
        $namaNya    = "Pembeli";
    }else{
        $namaNya    = $_SESSION['nama'];
    }

    $queryProfile   = mysqli_query($koneksinya, "SELECT * FROM profil WHERE id_profil='1'");
    $resultProfile  = mysqli_fetch_assoc($queryProfile);

    if (isset($_POST['pesan_produk'])) {
        $id_transaksi   = "TRA".$_SESSION['id_pembeli'].rand(00,99);
        $id_pembeli     = $_SESSION['id_pembeli'];
        $id_produk      = $_POST['id_produk'];
        $jumlah         = $_POST['jumlah'];
        $total_bayar    = $_POST['harga']*$jumlah;
        $status         = "Booking";
        $waktu          = date("d M Y H:i:s");

        $rowspembeli      = mysqli_query($koneksinya, "INSERT INTO transaksi VALUES ('$id_transaksi', '$id_pembeli', '$id_produk', '$jumlah', '$total_bayar', '$status', '$waktu')");
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>PESANAN SAYA - <?= $namaNya; ?></title>
    <link rel="stylesheet" href="plugins/css/bootstrap.css">
    <link rel="stylesheet" href="plugins/css/heroic-features.css">
    <link rel="stylesheet" href="plugins/css/font-awesome.min.css">
</head>

<body class="bg-light">

    <!-- Navigation -->
    <?php require 'navigation.php'; ?>

    <!-- Page Content -->
    <div class="container">

        <!-- Jumbotron Header -->
        <header class="jumbotron my-4">
            <h1 class="display-3">Hallo <u><?= $namaNya; ?></u>!</h1>
            <p class="lead">Ini adalah daftar pesanan anda</p>
        </header>

        <!-- Page Features -->
        <div class="row text-center">
            <div class="col-md-12">
                <table class="table table-striped table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th style="width: 5%">No</th>
                            <th style="width: 15%">ID Transaksi</th>
                            <th style="width: 25%">Nama Produk</th>
                            <th style="width: 10%">Jumlah</th>
                            <th style="width: 20%">Total Bayar</th>
                            <th style="width: 20%">Waktu Pesan</th>
                            <th style="width: 15%">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                            $nomor = 1;
                            $queryTransaksi    = "SELECT transaksi.id_transaksi, transaksi.id_pembeli, transaksi.jumlah, transaksi.total_bayar, transaksi.status, transaksi.waktu, produk.nama_produk FROM transaksi INNER JOIN produk ON transaksi.id_produk = produk.id_produk WHERE transaksi.id_pembeli='$_SESSION[id_pembeli]' ORDER BY transaksi.waktu DESC";
                            $rowTransaksi      = mysqli_query($koneksinya, $queryTransaksi);
                            while ($resultTransaksi   = mysqli_fetch_assoc($rowTransaksi)) {

                        ?>
                        <tr>
                            <td style="width: 5%"><?= $nomor++; ?></td>
                            <th style="width: 15%"><?= $resultTransaksi['id_transaksi']; ?></th>
                            <td style="width: 25%"><?= $resultTransaksi['nama_produk']; ?></td>
                            <td style="width: 10%"><?= $resultTransaksi['jumlah']; ?></td>
                            <td style="width: 25%"><strong class="text-success">Rp<?= rp($resultTransaksi['total_bayar']); ?></strong></td>
                            <td style="width: 20%"><?= $resultTransaksi['waktu']; ?></td>
                            <td style="width: 25%">
                                <?php 
                                    if ($resultTransaksi['status']=="Booking") {
                                       echo "<button type='button' class='btn btn-warning'>Booking <i class='fa fa-book'></i></button>";
                                    }elseif ($resultTransaksi['status']=="Pengiriman") {
                                        echo "<button type='button' class='btn btn-primary'>Pengiriman <i class='fa fa-plane'></i></button>";
                                    }elseif ($resultTransaksi['status']=="Selesai") {
                                        echo "<button type='button' class='btn btn-success'>Selesai <i class='fa fa-check-square-o'></i></button>";
                                    }
                                ?>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <?php require 'footer.php'; ?>

    <script src="plugins/js/jquery.min.js"></script>
    <script src="plugins/js/popper.js"></script> 
    <script src="plugins/js/bootstrap.min.js"></script>

    <script type="text/javascript">

        // Popover
        $(document).ready(function(){
            $('[data-toggle="popover"]').popover();   
        });
        // Popover

    </script>

</body>
</html>