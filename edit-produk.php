<?php 

    // Mmemanggil connect database
    include '../connect.php';

    session_start();

    if (empty($_SESSION['id_admin'])) {
        header('location:logout.php');
    }

    $queryProduk    = "SELECT * FROM produk WHERE id_produk='$_GET[id]'";
    $rowProduk      = mysqli_query($koneksinya, $queryProduk);
    $resultProduk   = mysqli_fetch_assoc($rowProduk);

    if (isset($_POST['selesai'])) {
        $nama_produkNya = $_POST['nama_produk'];
        $hargaNya       = $_POST['harga'];
        $stockNya       = $_POST['stock'];

        // Include Gambar
        $nama_gambarNya    = $_FILES['gambar']['name']; // mendapatkan nama gambar

        if (empty($nama_gambarNya)) {
            $nama_gambar_edit   = $resultProduk['gambar'];
        }else{
            $nama_gambar_edit   = $nama_gambarNya;
            $lokasi_gambarNya   = $_FILES['gambar']['tmp_name']; // mendapatkan lokasi gambar
            $tujuan_gambarNya   = '../gambar-produk'; // pindah gambar tersebut ke lokasi ini
            $upload_gambarNya   = move_uploaded_file($lokasi_gambarNya, $tujuan_gambarNya.'/'.$nama_gambarNya); // function mengupload/memindahkan file ke direktori yang di maksud
        }

        $queryEdit      = mysqli_query($koneksinya, "UPDATE produk SET nama_produk='$nama_produkNya', harga='$hargaNya', gambar='$nama_gambar_edit', stock='$stockNya' WHERE id_produk='$_GET[id]'");

        if (!empty($queryEdit)) {
            header('location:produk.php?page=produk');
        }
    }

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= $_SESSION['nama']; ?> - Edit Produk</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <?php require 'sidebar.php'; ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php require 'top-bar.php'; ?>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Edit Produk <u class="text-primary">"<?= $resultProduk['nama_produk']; ?>"</u></h1>
                    </div>

                    <!-- Content Row -->
                    <div class="row">

                        <!-- Earnings (Monthly) Card Example -->
                        <div class="col-md-12">
                            <div class="card border-left-primary shadow h-100 py-2">
                                <form action="" method="POST" class="col-12" enctype="multipart/form-data">
                                    <div class="p-5">
                                        <div class="text-center">
                                            <h1 class="h4 text-primary mb-4">Masukkan data di bawah ini dengan lengkap & benar!</h1>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama_produk">Nama Produk</label>
                                            <input type="text" id="nama_produk" class="form-control form-control-user" name="nama_produk" value="<?= $resultProduk['nama_produk']; ?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="harga">Harga Produk</label>
                                            <input type="number" id="harga" class="form-control form-control-user" name="harga" value="<?= $resultProduk['harga']; ?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="stock">Stock Produk</label>
                                            <input type="number" id="stock" class="form-control form-control-user" name="stock" value="<?= $resultProduk['stock']; ?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="gambar">Pilih Gambar</label>
                                            <br>
                                            <img src="../gambar-produk/<?= $resultProduk['gambar']; ?>" alt="<?= $resultProduk['nama_produk']; ?>" style="width: 150px;height: 200px;">
                                            <br>
                                            <br>
                                            <input type="file" id="gambar" class="form-control-file  form-control-user" name="gambar">
                                        </div>
                                        <button type="submit" name="selesai" class="btn btn-primary btn-user btn-block">SELESAI <i class="fa fa-check"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Your Website 2021</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

</body>

</html>