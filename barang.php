<?php 

   
    include '../../ini-koneksi-database.php';

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>MELATI</title>
   
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
   
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
    
    <link rel="stylesheet" href="../dist/css/skins/skin-blue.min.css">

    
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    
    <header class="main-header">

       
        <a href="index.php" class="logo">
           
            <span class="logo-mini"><b>ADM</b></span>
            
            <span class="logo-lg"><b>Admin</b></span>
        </a>

        
        <nav class="navbar navbar-static-top" role="navigation">
           
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
        </nav>
    </header>

    
    <aside class="main-sidebar">

        
        <section class="sidebar">
            
            <ul class="sidebar-menu">
                <li class="header">Menu Utama</li>
               
                <li class="active"><a href="index.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
                <li><a href="kasir.php"><i class="fa fa-calculator"></i> <span>Kasir</span></a></li>
                <li><a href="barang.php"><i class="fa fa-list"></i> <span>Barang</span></a></li>
                <li><a href="pegawai.php"><i class="fa fa-list"></i> <span>Pegawai</span></a></li>
                <li><a href="pembeli.php"><i class="fa fa-list"></i> <span>Pembeli</span></a></li>
            </ul>
         
        </section>
    
    </aside>

    
    <div class="content-wrapper">

       
        <section class="content">

            <div class="box box-primary">

                <div class="box-header with-border">
                    <a href="add_barang.php" class="btn btn-block btn-success">TAMBAH BARANG</a>
                </div>

                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Barang</th>
                                <th>Jenis</th>
                                <th>Harga</th>
                                <th>Karat</th>
                                <th>Stok</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>

                        <tbody>

                            <?php

                                $no    = 1; 
                                $querybarang = "SELECT * FROM barang ORDER BY id_barang DESC";
                                $rowbarang   = mysqli_query($koneksi, $querybarang);
                                while ($resultbarang   = mysqli_fetch_assoc($rowbarang)) {

                            ?>

                            <tr>
                                <td><?php echo $no++; ?></td>
                                <th><?php echo $resultbarang['nama_barang']; ?></th>
                                <th><?php echo $resultbarang['jenis']; ?></th>
                                <th><?php echo $resultbarang['harga']; ?></th>
                                <th><?php echo $resultbarang['karat']; ?></th>
                                <th><?php echo $resultbarang['stok']; ?></th>
                                <th>
                                    <a href="edit_barang.php?id_barang=<?php echo $resultbarang['id_barang']; ?>" class="btn btn-primary">EDIT</a>
                                    <a onclick="return confirm('Apakah anda ingin menghapus data ini?');" href="proses_delete_barang.php?id_barang=<?php echo $resultbarang['id_barang']; ?>" class="btn btn-danger">Hapus</a>
                                </th>
                            </tr>

                            <?php
                                }
                            ?>

                        </tbody>
                    </table>
                </div>
            </div>

        </section>
        
    </div>
   

    
    <footer class="main-footer">
        
        <div class="pull-right hidden-xs">
            Anything you want
        </div>
        
        <strong>Copyright &copy; 2016 <a href="#">Company</a>.</strong> All rights reserved.
    </footer>

</div>

<script src="../plugins/jQuery/jquery-2.2.3.min.js"></script>

<script src="../bootstrap/js/bootstrap.min.js"></script>

<script src="../dist/js/app.min.js"></script>


</body>
</html>
