Baru < Aplikasi Toko Emas Berbasis Website >"

Aplikasi memudahkan Pemilik Toko Emas bisa mengakses/mengetahui langsung emas yang telah berhasil dijual serta pengasilannya, dan lebih efisien serta mudah ketika ingin mengetahui siapa yang membeli emas tersebut.
Aplikasi ini juga memudahkan pelanggan untuk memesan/membeli tidak harus ke Toko karena pandemi. Aplikasi ini terdiri dari Home, profil Toko Emas, kasir, pembeli, pegawai, barang.

==================================================
WEBSITE UTAMA

Index
	- Profile Toko Emas
	- Produk
	- Pesanan Saya
	- Profile
		- Logout

==================================================
WEBSITE ADMIN
	- Dashboard
		- Barang Terjual
		- Total Penghasilan

	- Profile

	- Transaksi

	- Produk
		- Tambah Produk
		- Edit Produk
		- Hapus Produk

	- Data Pembeli

	- Data Pegawai

==================================================
DATABASE
	- Admin
		- id_admin
		- username
		- password

	- Produk
		- id_produk
		- nama_produk
		- harga
		- gambar
		- stock

	- Pembeli
		- id_pembeli
		- nama
		- alamat
		- username
		- password

	- Transaksi
		- id_transaksi (char 6)
		- id_pembeli
		- id_produk
		- jumlah
		- total_bayar
		- status (Booking, Pengiriman, Selesai)
		- waktu (date)

	- Profil
		- id_profil
		- deskripsi