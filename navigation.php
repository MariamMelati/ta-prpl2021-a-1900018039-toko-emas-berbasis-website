<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="index.php">Toko Emas</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="index.php"><i class="fa fa-home"></i> Home
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="produk.php"><i class="fa fa-file-text-o"></i> Produk</a>
                </li>
                <li class="nav-item">
                    <?php
                        if (empty($_SESSION['id_pembeli'])) {
                            echo "<a class='nav-link' href='masuk.php'><i class='fa fa-shopping-bag'></i> Pesanan Saya</a>";
                        }else{
                            echo "<a class='nav-link' href='pesanan-saya.php'><i class='fa fa-shopping-bag'></i> Pesanan Saya</a>";
                        }
                    ?>
                    
                </li>
                <form class="form-inline" action="cari-produk.php" method="POST">
                    <input class="form-control mr-sm-2" type="search" placeholder="Cari produk..." name="input_cari" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit" name="cariproduk"><i class="fa fa-search"></i></button>
                </form>
                <!-- Example split danger button -->
                <div class="btn-group ml-4">
                    <button type="button" class="btn btn-danger"><i class="fa fa-user"></i> Akun</button>
                    <button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#"><?= $namaNya; ?></a>
                        <div class="dropdown-divider"></div>

                        <?php
                            if (empty($_SESSION['id_pembeli'])) {
                                echo "<a class='dropdown-item' href='masuk.php'><i class='fa fa-sign-in'></i> Masuk</a>";
                                echo "<a class='dropdown-item' href='daftar.php'><i class='fa fa-pencil-square-o'></i> Daftar</a>";
                            }else{
                                echo "<a class='dropdown-item' href='keluar.php'><i class='fa fa-sign-out'></i> Keluar</a>";
                            }
                        ?>
                        
                    </div>
                </div>
            </ul>
        </div>
    </div>
</nav>