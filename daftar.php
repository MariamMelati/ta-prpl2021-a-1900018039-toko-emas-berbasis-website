<?php 

    // Mmemanggil connect database
    include 'connect.php';

    if (isset($_POST['daftar'])) {
        $namaNya        = $_POST['nama'];
        $alamatNya      = $_POST['alamat'];
        $usernameNya    = $_POST['username'];
        $passwordnya    = $_POST['password'];

        $rowspembeli      = mysqli_query($koneksinya, "INSERT INTO pembeli VALUES ('', '$namaNya', '$alamatNya', '$usernameNya', '$passwordnya')");

        if (!empty($rowspembeli)) {

            // jika berhasil daftar
            // dan akan di redirect ke halaman dashboard pembeli
            header('location:masuk.php');
            
        }
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>DAFTAR AKUN - TOKO EMAS</title>
    <link rel="stylesheet" href="plugins/css/bootstrap.css">
    <link rel="stylesheet" href="plugins/css/heroic-features.css">
    <link rel="stylesheet" href="plugins/css/font-awesome.min.css">
</head>

<body class="bg-light">

    <div class="row">
        <div class="col-md-6 offset-md-3">
            <!-- form card login with validation feedback -->
            <div class="card card-outline-secondary">
                <div class="card-header">
                    <h3 class="mb-0">Form Daftar Akun</h3>
                </div>
                <div class="card-body">
                    <form class="form" action="" method="POST">

                        <?php

                            if (isset($_POST['daftar'])) {
                                if (empty($rowspembeli)) {
                                    echo "<div class='alert alert-danger text-left' role='alert'>";
                                    echo "<h4 class='alert-heading'><i class='fa fa-exclamation-triangle'></i> GAGAL!</h4>";
                                    echo "<hr>";
                                    echo "<p class='mb-0'>Mohon masukkan username & password dengan benar!</p>";
                                    echo "</div>";
                                }
                            }

                        ?>

                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input type="text" class="form-control" name="nama" id="nama" required>
                        </div>
                        <div class="form-group">
                            <label for="alamat">Alamat</label>
                            <textarea class="form-control" id="alamat" name="alamat" rows="3" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="uname1">Username</label>
                            <input type="text" class="form-control" name="username" id="uname1" required>
                            <div class="invalid-feedback">Please enter your username</div>
                        </div>
                        <div class="form-group">
                            <label for="pwd1">Password</label>
                            <input type="password" class="form-control" id="pwd1" name="password" required autocomplete="new-password">
                            <div class="invalid-feedback">Please enter a password</div>
                        </div>
                        <button type="submit" name="daftar" class="btn btn-success btn-block btn-lg mb-2">Daftar <i class="fa fa-pencil-square-o"></i></button>

                        <p class="text-center">Sudah punya akun?</p>

                        <a href="masuk.php" role="button" class="btn btn-success btn-block btn-lg mb-2">Masuk <i class="fa fa-sign-in"></i></a>
                    </form>
                </div>
                <!--/card-body-->
            </div>
            <!-- /form card login -->

        </div>
    </div>

    <script src="plugins/js/jquery.min.js"></script>
    <script src="plugins/js/popper.js"></script> 
    <script src="plugins/js/bootstrap.min.js"></script>

    <script type="text/javascript">

        // Popover
        $(document).ready(function(){
            $('[data-toggle="popover"]').popover();   
        });
        // Popover

    </script>

</body>
</html>