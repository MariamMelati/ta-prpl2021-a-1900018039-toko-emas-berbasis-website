<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="dashboard.php?page=dashboard">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">SB Admin <sup>2</sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item <?php if($_GET['page']=='dashboard'){ echo 'active'; } ?>">
        <a class="nav-link" href="dashboard.php?page=dashboard">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <li class="nav-item <?php if($_GET['page']=='profile'){ echo 'active'; } ?>">
        <a class="nav-link" href="profile.php?page=profile">
            <i class="fas fa-fw fa-id-card"></i>
            <span>Profile</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Menu Utama
    </div>

    <li class="nav-item <?php if($_GET['page']=='pesanan-masuk' OR $_GET['page']=='pesanan-dikirim' OR $_GET['page']=='pesanan-selesai'){ echo 'active'; } ?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseOne"
            aria-expanded="true" aria-controls="collapseOne">
            <i class="fas fa-fw fa-bell"></i>
            <span>Transaksi</span></a>
        </a>
        <div id="collapseOne" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item <?php if($_GET['page']=='pesanan-masuk'){ echo 'active'; } ?>" href="pesanan-masuk.php?page=pesanan-masuk"><i class="fas fa-fw fa-file-import"></i> Pesanan Masuk</a>
                <a class="collapse-item <?php if($_GET['page']=='pesanan-dikirim'){ echo 'active'; } ?>" href="pesanan-dikirim.php?page=pesanan-dikirim"><i class="fas fa-fw fa-plane-departure"></i> Pesanan Dikirim</a>
                <a class="collapse-item <?php if($_GET['page']=='pesanan-selesai'){ echo 'active'; } ?>" href="pesanan-selesai.php?page=pesanan-selesai"><i class="fas fa-fw fa-clipboard-check"></i> Pesanan Selesai</a>
            </div>
        </div>
    </li>

    <li class="nav-item <?php if($_GET['page']=='produk' OR $_GET['page']=='tambah-produk' OR $_GET['page']=='edit-produk'){ echo 'active'; } ?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
            aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-clipboard-list"></i>
            <span>Produk</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item <?php if($_GET['page']=='produk' OR $_GET['page']=='edit-produk'){ echo 'active'; } ?>" href="produk.php?page=produk"><i class="fas fa-fw fa-folder-open"></i> Daftar Produk</a>
                <a class="collapse-item <?php if($_GET['page']=='tambah-produk'){ echo 'active'; } ?>" href="tambah-produk.php?page=tambah-produk"><i class="fas fa-fw fa-folder-plus"></i> Tambah Produk</a>
            </div>
        </div>
    </li>

    <li class="nav-item <?php if($_GET['page']=='data-pembeli'){ echo 'active'; } ?>">
        <a class="nav-link" href="data-pembeli.php?page=data-pembeli">
            <i class="fas fa-fw fa-users"></i>
            <span>Data Pembeli</span></a>
    </li>

    <li class="nav-item <?php if($_GET['page']=='data-pegawai'){ echo 'active'; } ?>">
        <a class="nav-link" href="data-pegawai.php?page=data-pegawai">
            <i class="fas fa-fw fa-people-carry"></i>
            <span>Data Pegawai</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>